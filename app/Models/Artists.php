<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Artist extends Model
{
    use HasFactory;

    protected $table = 'artists';

    public function music() {
        return $this->hasMany(Music::class);
    }

    public function album() {
        return $this->belongsToMany(Album::class);
    }

}
