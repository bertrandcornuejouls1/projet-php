<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Album extends Model
{
    use HasFactory;

    protected $table = 'albums';

    public function music() {
        return $this->hasOne(Music::class);
    }

    public function artist() {
        return $this->belongsToMany(Artist::class);
    }

    
    
}
