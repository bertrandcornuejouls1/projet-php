<?php

class CategorieController extends
Controller
{
/**
* Display a listing of the resource.
*
* @return \Illuminate\Http\Response
*/
    public function index()
    {
        $categories = Categorie::all();
        return $categories;
    }

}
