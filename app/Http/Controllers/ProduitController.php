<?php

class ProduitController extends Controller
{
    /**
    * Fonction pour récupérer tous les produits d’une famille.
    */
    public function getProduitsByFamille($famille_id)
    {
        return Produit::where(‘famille_id', $ famille_id)->get();
    }         
}