<?php

namespace Database\Seeders;

use App\Models\user;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder{

    public function run(){
        $user = new User();
        $user->email = "bertrand.cornuejouls@orange.fr";
        $user->name = "beber";
        $user->password = Hash::make("1234");
        $user->save();

    }


}
