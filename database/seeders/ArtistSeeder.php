<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class ArtistsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $artist = Artist::factory()->make();
        $artist->save();
        
    }
}
