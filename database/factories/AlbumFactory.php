<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class AlbumsFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
            'name' => $this->faker->word(),
            'cover_path' => $this->faker->imageUrl(100, 800, "Album"),
            'year' => $this->faker->numberBetween(1950,2023),
        ];
    }
}
