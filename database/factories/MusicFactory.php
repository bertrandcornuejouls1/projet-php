<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class MusicFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
            'track' => $this->faker->numberBetween(1950,2023),
            'title' => $this->faker->word(),
            'artist' => $this->faker->word(),
            'album' => $this->faker->word(),
            'style' => $this->faker->word(),
            'duration' => $this->faker->numberBetween(0,10),
            'year' => $this->faker->numberBetween(1950,2023),
            'isFavorite' => $this->faker->word(),
            'file_path' => $this->faker->Url(100, 800, "Music"),
        ];
    }
}
