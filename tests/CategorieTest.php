<?php

class CategorieTest extends TestCase
{

    public function test_get_albums()
    {
        $response = $this->get('api/albums');
        $response->assertStatus(200);
    }


    public function test_create_categorie()
    {
        $response = $this->postJson('api/categories', 
    ['nom' => "Test", 'ordre' => 1]);
    $response->assertStatus(201);
    }

}